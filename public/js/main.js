/*var queryString = window.location.search;
var header = document.querySelector(".header");*/
var el = document.querySelector('.toggle-me');
var element = document.querySelector('.theme');
var dataAttribute = element.getAttribute('data-theme');


el.addEventListener('click', function () {
  
  //element.setAttribute("data-theme", "dark");
  element.setAttribute('data-theme',  element.getAttribute('data-theme') === 'dark' ? 'light' : 'dark')
  el.classList.toggle('light');
  el.classList.toggle('dark');
  document.querySelector(".sun-logo").classList.toggle("animate-sun");
  document.querySelector(".moon-logo").classList.toggle("animate-moon");
});
